function values(data) {
  if (typeof data == "object") {
    const result = [];

    for (let item in data) {
      result.push(data[item]);
    }

    return result;
  } else {
    return [];
  }
}
module.exports = values