function defaults(data, change) {
  if (typeof data == "object" && typeof change == "object") {
    for (let item in change) {
      data[item] = change[item];
    }

    return data;
  } else {
    return {};
  }
}

module.exports = defaults;
