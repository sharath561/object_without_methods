//const testObject = require('./testObject')

function mapObject(data,cb) {
  if (typeof data == "object" && cb != undefined) {

    let valuesData = {};


    for (let item in data) {
      
        valuesData = cb(item,data,valuesData)
    }


    return valuesData;
  } else {
    return [];
  }
}


module.exports = mapObject;
