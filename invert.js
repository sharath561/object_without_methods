function invert(data) {
  if (typeof data == "object") {
    const result = {};

    for (let item in data) {
      result[data[item]] = item;
    }

    return result;
  }else{
    return {}
  }
}

module.exports = invert;
