function pairs(data) {
  if (typeof data == "object") {
    const result = [];

    for (let item in data) {
      result.push([item, data[item]]);
    }

    return result;
  }
  else{

    return []
  }
}

module.exports = pairs;
